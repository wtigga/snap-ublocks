/*

    ublocks.js

    µBlocks device support for Snap!


    written by John Maloney, Jens Mönig, and Bernat Romagosa
    http://microblocks.fun

    Copyright (C) 2018 by John Maloney, Jens Mönig, Bernat Romagosa

    This file is part of Snap!.

    Snap! is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


    credits
    -------
    John Maloney designed the original µblocks bytecode system

*/

/*global modules, CommandBlockMorph, ReporterBlockMorph, BlockMorph,
DialogBoxMorph, contains, List, SpriteMorph, localize,
StageMorph, MenuMorph, IDE_Morph, ToggleMorph, nop, TextMorph, PushButtonMorph,
VariableDialogMorph, BlockDialogMorph, Color, TableDialogMorph, HatBlockMorph,
SyntaxElementMorph, BlockEditorMorph, InputSlotMorph, RingMorph,
ReporterSlotMorph, CommandSlotMorph, SpriteIconMorph, Compiler*/

modules.ublocks = '2018-May-15';

var DeviceMorph;
var Postal;
var Compiler;

var MicroBlocksGlobals = {}; // added by John (must persist beyond the life of a single Compiler instance)

// Global stuff ////////////////////////////////////////////////////////

Array.prototype.toHexString = function () {
    return (this.map(function (each) { return each.toString(16); })).toString();
};

function listify (array) {
    var result = new List(array),
        i, el;
    for (i = 1; i <= result.length(); i += 1) {
        el = result.at(i);
        if (el instanceof Array) {
            result.put(listify(el), i);
        }
    }
    return result;
};


// DeviceMorph //////////////////////////////////////////////////

DeviceMorph.prototype = new SpriteMorph();
DeviceMorph.prototype.constructor = DeviceMorph;
DeviceMorph.uber = SpriteMorph.prototype;

function DeviceMorph(globals) {
    this.init(globals);
};

DeviceMorph.prototype.init = function (globals) {
    DeviceMorph.uber.init.call(this, globals);
    this.fps = 2;
    this.name = localize('Device');
    this.watcherValues = {};
    this.watchedVars = [];
    this.portPath = null;
    this.vmVersion = null;
    this.boardType = null;
};

DeviceMorph.prototype.categories =
    [
        'control',
        'input/output',
        'operators',
        'variables'
    ];

SpriteMorph.prototype.blockColor['input/output'] = new Color(240, 54, 48);

// Board colors
// Only needed for boards with a specific block library. For now only the
// micro:bit does, but I'm adding some more just to test the feature.
SpriteMorph.prototype.blockColor['micro:bit'] = new Color(51, 184, 242);
SpriteMorph.prototype.blockColor['Due'] = new Color(24, 167, 181);
SpriteMorph.prototype.blockColor['M0'] = new Color(24, 167, 181);
SpriteMorph.prototype.blockColor['Zero'] = new Color(24, 167, 181);
SpriteMorph.prototype.blockColor['ESP8266'] = new Color(0, 84, 255);

DeviceMorph.prototype.disable = function () {
    // We lost connection to the physical device, so we need to
    // disable ourselves
    this.scripts.forAllChildren(function (script) {
        script.alpha = 0.5;
        script.changed();
    });
    this.scripts.changed();
};

DeviceMorph.prototype.enable = function () {
    // We recovered the connection to the physical device
    this.scripts.forAllChildren(function (script) {
        script.alpha = 1;
        script.changed();
    });
    this.scripts.changed();
};

DeviceMorph.prototype.initBlocks = function (globals) {
    DeviceMorph.prototype.blocks = {
        analogReadOp: {
            only: DeviceMorph,
            type: 'reporter',
            category: 'input/output',
            spec: 'read analog pin %n',
            defaults: [1]
        },
        analogWriteOp: {
            only: DeviceMorph,
            type: 'command',
            category: 'input/output',
            spec: 'set analog pin %n to %n',
            defaults: [1, 1023]
        },
        digitalReadOp: {
            only: DeviceMorph,
            type: 'predicate',
            category: 'input/output',
            spec: 'read digital pin %n',
            defaults: [1]
        },
        digitalWriteOp: {
            only: DeviceMorph,
            type: 'command',
            category: 'input/output',
            spec: 'set digital pin %n to %b',
            defaults: [1, true]
        },
        setUserLED: {
            only: DeviceMorph,
            type: 'command',
            category: 'input/output',
            spec: 'set user LED %b',
            defaults: [true]
        },
        microsOp: {
            only: DeviceMorph,
            type: 'reporter',
            category: 'input/output',
            spec: 'micros'
        },
        millisOp: {
            only: DeviceMorph,
            type: 'reporter',
            category: 'input/output',
            spec: 'millis'
        },
        i2cSet: {
            only: DeviceMorph,
            type: 'command',
            category: 'input/output',
            spec: 'i2c set device %n register %n to %n',
            defaults: [0, 0, 0]
        },
        i2cGet: {
            only: DeviceMorph,
            type: 'reporter',
            category: 'input/output',
            spec: 'i2c get device %n register %n',
            defaults: [0, 0]
        },
        noop: {
            only: DeviceMorph,
            type: 'command',
            category: 'control',
            spec: 'no op'
        },
        peek: {
            only: DeviceMorph,
            type: 'reporter',
            category: 'input/output',
            spec: 'memory at %n',
            defaults: [0]
        },
        poke: {
            only: DeviceMorph,
            type: 'command',
            category: 'input/output',
            spec: 'set memory at %n to %n',
            defaults: [0, 0]
        },
        receiveGo: {
            type: 'hat',
            category: 'control',
            spec: 'when %greenflag clicked'
        },
        receiveMessage: {
            type: 'hat',
            category: 'control',
            spec: 'when I receive %msgHat'
        },
        doBroadcast: {
            type: 'command',
            category: 'control',
            spec: 'broadcast %msg'
        },
        whenReceiveCondition: {
            type: 'hat',
            category: 'control',
            spec: 'when %b'
        },
        waitMicros: {
            only: DeviceMorph,
            type: 'command',
            category: 'control',
            spec: 'wait %n microsecs',
            defaults: [10000]
        },
        waitMillis: {
            only: DeviceMorph,
            type: 'command',
            category: 'control',
            spec: 'wait %n millisecs',
            defaults: [500]
        },
        doWaitUntil: {
            type: 'command',
            category: 'control',
            spec: 'wait until %b'
        },
        doForever: {
            type: 'command',
            category: 'control',
            spec: 'forever %c'
        },
        doRepeat: {
            type: 'command',
            category: 'control',
            spec: 'repeat %n %c',
            defaults: [10]
        },
		// +++ 'while' is deprecated; use 'until' loop instead
        doWhile: {
            type: 'command',
            category: 'control',
            spec: 'while %b %c'
        },
        doUntil: {
            type: 'command',
            category: 'control',
            spec: 'repeat until %b %c'
        },
        doFor: {
            type: 'command',
            category: 'control',
            spec: 'for %upvar in %n %c',
            defaults: ['i', 10]
        },
        doIf: {
            type: 'command',
            category: 'control',
            spec: 'if %b %c'
        },
        doIfElse: {
            type: 'command',
            category: 'control',
            spec: 'if %b %c else %c'
        },
        doReport: {
            type: 'command',
            category: 'control',
            spec: 'report %s'
        },
        doStopDevice: {
            only: DeviceMorph,
            type: 'command',
            category: 'control',
            spec: 'stop all %stop'
        },
        reportSum: {
            type: 'reporter',
            category: 'operators',
            spec: '%n + %n'
        },
        reportDifference: {
            type: 'reporter',
            category: 'operators',
            spec: '%n \u2212 %n',
            alias: '-'
        },
        reportProduct: {
            type: 'reporter',
            category: 'operators',
            spec: '%n \u00D7 %n',
            alias: '*'
        },
        reportQuotient: {
            type: 'reporter',
            category: 'operators',
            spec: '%n / %n' // '%n \u00F7 %n'
        },
        reportDeviceEquals: {
            type: 'predicate',
            category: 'operators',
            spec: '%s = %s'
        },
        reportDeviceLessThan: {
            only: DeviceMorph,
            type: 'predicate',
            category: 'operators',
            spec: '%n < %n',
            defaults: [3, 4]
        },
        reportDeviceGreaterThan: {
            only: DeviceMorph,
            type: 'predicate',
            category: 'operators',
            spec: '%n > %n',
            defaults: [3, 4]
        },
        reportAnd: {
            type: 'predicate',
            category: 'operators',
            spec: '%b and %b'
        },
        reportOr: {
            type: 'predicate',
            category: 'operators',
            spec: '%b or %b'
        },
        reportNot: {
            type: 'predicate',
            category: 'operators',
            spec: 'not %b'
        },
        reportBoolean: {
            type: 'predicate',
            category: 'operators',
            spec: '%bool',
            alias: 'true boolean'
        },
        doSetVar: {
            type: 'command',
            category: 'variables',
            spec: 'set %var to %s',
            defaults: [null, 0]
        },
        doChangeVar: {
            type: 'command',
            category: 'variables',
            spec: 'change %var by %n',
            defaults: [null, 1]
        },
        doDeclareVariables: {
            type: 'command',
            category: 'other',
            spec: 'script variables %scriptVars'
        },
        newArray: {
            only: DeviceMorph,
            type: 'reporter',
            category: 'lists',
            spec: 'new array %n',
            defaults: [10]
        },
        newByteArray: {
            only: DeviceMorph,
            type: 'reporter',
            category: 'lists',
            spec: 'new byte array %n',
            defaults: [10]
        },
        fillArray: {
            only: DeviceMorph,
            type: 'command',
            category: 'lists',
            spec: 'fill %l with %n',
            defaults: [null, 0]
        },
        reportListItem: {
            type: 'reporter',
            category: 'lists',
            spec: 'item %idx of %l',
            defaults: [1]
        },
         doReplaceInList: {
            type: 'command',
            category: 'lists',
            spec: 'replace item %idx of %l with %s',
            defaults: [1, null, localize('thing')]
        },

		// micro:bit primitives for demos

        mbDisplay: {
            only: DeviceMorph,
            type: 'command',
            category: 'micro:bit',
            spec: 'display %br %b %b %b %b %b %br %b %b %b %b %b %br %b %b %b %b %b %br %b %b %b %b %b %br %b %b %b %b %b'
        },
        mbDisplayOff: {
            only: DeviceMorph,
            type: 'command',
            category: 'micro:bit',
            spec: 'clear display'
        },
        mbPlot: {
            only: DeviceMorph,
            type: 'command',
            category: 'micro:bit',
            spec: 'plot x: %n y: %n',
            defaults: [3, 3]
        },
        mbUnplot: {
            only: DeviceMorph,
            type: 'command',
            category: 'micro:bit',
            spec: 'unplot x: %n y: %n',
            defaults: [3, 3]
        },
        mbTiltX: {
            only: DeviceMorph,
            type: 'reporter',
            category: 'micro:bit',
            spec: 'tilt x'
        },
        mbTiltY: {
            only: DeviceMorph,
            type: 'reporter',
            category: 'micro:bit',
            spec: 'tilt y'
        },
        mbTiltZ: {
            only: DeviceMorph,
            type: 'reporter',
            category: 'micro:bit',
            spec: 'tilt z'
        },
        mbTemp: {
            only: DeviceMorph,
            type: 'reporter',
            category: 'micro:bit',
            spec: 'temperature Celsius'
        },
    	buttonA: {
        	only: DeviceMorph,
            type: 'predicate',
            category: 'micro:bit',
            spec: 'Button A pressed ?'
        },
        buttonB: {
            only: DeviceMorph,
            type: 'predicate',
            category: 'micro:bit',
            spec: 'Button B pressed ?'
        },

        random: {
            only: DeviceMorph,
            type: 'reporter',
            category: 'operators',
            spec: 'random between 1 and %n',
            defaults: [5]
        }

    };
};

DeviceMorph.prototype.initBlocks();

DeviceMorph.prototype.blockTemplates = function (category) {
    var blocks = [], myself = this, varNames, button,
        cat = category || 'input/output', txt,
        inheritedVars = this.inheritedVariableNames(),
        toggles = {};

    function block(selector, isGhosted, toggle) {
        if (StageMorph.prototype.hiddenPrimitives[selector]) {
            return null;
        }
        var newBlock = DeviceMorph.prototype.blockForSelector(selector, true);
        newBlock.isTemplate = true;
        newBlock.watcherToggle = toggle;
        if (isGhosted) {newBlock.ghost(); }
        return newBlock;
    }

    function variableBlock(varName) {
        var newBlock = DeviceMorph.prototype.variableBlock(varName);
        newBlock.isDraggable = false;
        newBlock.isTemplate = true;
        if (contains(inheritedVars, varName)) {
            newBlock.ghost();
        }
        return newBlock;
    }

    function watcherToggle(selector, isMultiple) {
        var reporter, args;
        if (StageMorph.prototype.hiddenPrimitives[selector]) {
            return null;
        }
        var info = DeviceMorph.prototype.blocks[selector];
        return new ToggleMorph(
            'checkbox',
            this,
            function () {
                if (isMultiple) {
                    reporter = detect(blocks, function (each) { return each.selector === selector; });
                    if (reporter) {
                        args = reporter.inputs().map(function (each) { return each.contents().text; });
                    }
                }
                myself.toggleWatcher(
                    selector,
                    localize(info.spec),
                    myself.blockColor[info.category],
                    args // will be undefined for reporters without inputs
                );
            },
            null,
            function () {
                if (isMultiple) {
                    reporter = detect(blocks, function (each) { return each.selector === selector; });
                    if (reporter) {
                        args = reporter.inputs().map(function (each) { return each.contents().text; });
                    }
                }
                return myself.showingWatcher(selector, args);
            },
            null
        );
    }

    function variableWatcherToggle(varName) {
        return new ToggleMorph(
            'checkbox',
            this,
            function () {
                myself.toggleVariableWatcher(varName);
            },
            null,
            function () {
                return myself.showingVariableWatcher(varName);
            },
            null
        );
    }

    function helpMenu() {
        var menu = new MenuMorph(this);
        menu.addItem('help...', 'showHelp');
        return menu;
    }

    function addVar(pair) {
        var ide;
        if (pair) {
            if (myself.isVariableNameInUse(pair[0], pair[1])) {
                myself.inform('that name is already in use');
            } else {
                ide = myself.parentThatIsA(IDE_Morph);
                myself.addVariable(pair[0], pair[1]);
                if (!myself.showingVariableWatcher(pair[0])) {
                    myself.toggleVariableWatcher(pair[0], pair[1]);
                }
                ide.flushBlocksCache('variables'); // b/c of inheritance
                ide.refreshPalette();
            }
        }
    }

    toggles['analogReadOp'] = watcherToggle('analogReadOp', true);
    toggles['digitalReadOp'] = watcherToggle('digitalReadOp', true);

    if (cat === 'control') {
        blocks.push(block('receiveGo'));
        blocks.push(block('whenReceiveCondition'));
        blocks.push(block('receiveMessage'));
        blocks.push(block('doBroadcast'));
        blocks.push('-');
        blocks.push(block('waitMicros'));
        blocks.push(block('waitMillis'));
        blocks.push(block('doWaitUntil'));
        blocks.push('-');
        blocks.push(block('doForever'));
        blocks.push(block('doRepeat'));
        blocks.push(block('doWhile'));
        blocks.push(block('doUntil'));
        blocks.push(block('doFor'));
        blocks.push('-');
        blocks.push(block('doIf'));
        blocks.push(block('doIfElse'));
        blocks.push('-');
        blocks.push(block('doReport'));
        blocks.push('-');
        blocks.push(block('doStopDevice'));
    // TODO in the future:
    // } else if (cat === this.boardType) {
    // and the blocks collection should be dependant on the board type
    } else if (cat === 'micro:bit' && this.boardType === 'micro:bit') {
        blocks.push(block('mbDisplay'));
        blocks.push(block('mbDisplayOff'));
        blocks.push('-');
        blocks.push(block('mbPlot'));
        blocks.push(block('mbUnplot'));
        blocks.push('-');
        blocks.push(watcherToggle('buttonA'));
        blocks.push(block('buttonA'));
        blocks.push(watcherToggle('buttonB'));
        blocks.push(block('buttonB'));
        blocks.push('-');
        blocks.push(watcherToggle('mbTiltX'));
        blocks.push(block('mbTiltX'));
        blocks.push(watcherToggle('mbTiltY'));
        blocks.push(block('mbTiltY'));
        blocks.push(watcherToggle('mbTiltZ'));
        blocks.push(block('mbTiltZ'));
        blocks.push('-');
        blocks.push(watcherToggle('mbTemp'));
        blocks.push(block('mbTemp'));
        blocks.push('-');
    } else if (cat === 'input/output') {
        blocks.push(toggles['analogReadOp']);
        blocks.push(block('analogReadOp', false, toggles['analogReadOp']));
        blocks.push(block('analogWriteOp'));
        blocks.push('-');
        blocks.push(toggles['digitalReadOp']);
        blocks.push(block('digitalReadOp', false, toggles['digitalReadOp']));
        blocks.push(block('digitalWriteOp'));
        blocks.push('-');
        blocks.push(block('setUserLED'));
        blocks.push('-');
        blocks.push(watcherToggle('microsOp'));
        blocks.push(block('microsOp'));
        blocks.push(watcherToggle('millisOp'));
        blocks.push(block('millisOp'));
        blocks.push('-');
        blocks.push(block('i2cSet'));
        blocks.push(block('i2cGet'));
        blocks.push('-');
        blocks.push(block('peek'));
        blocks.push(block('poke'));
    } else if (cat === 'operators') {
        blocks.push(block('reportSum'));
        blocks.push(block('reportDifference'));
        blocks.push(block('reportProduct'));
        blocks.push(block('reportQuotient'));
        blocks.push('-');
        blocks.push(block('random'));
        blocks.push('-');
        blocks.push(block('reportDeviceEquals'));
        blocks.push(block('reportDeviceLessThan'));
        blocks.push(block('reportDeviceGreaterThan'));
        blocks.push('-');
        blocks.push(block('reportNot'));
        blocks.push('-');
        blocks.push(block('reportAnd'));
        blocks.push(block('reportOr'));
        blocks.push(block('reportBoolean'));
    } else if (cat === 'variables') {

        button = new PushButtonMorph(
                null,
                function () {
                    new VariableDialogMorph(
                        null,
                        addVar,
                        myself
                        ).prompt(
                            'Variable name',
                            null,
                            myself.world()
                        );
                },
                'Make a variable'
            );
        button.userMenu = helpMenu;
        button.selector = 'addVariable';
        button.showHelp = BlockMorph.prototype.showHelp;
        blocks.push(button);

        if (this.deletableVariableNames().length > 0) {
            button = new PushButtonMorph(
                    null,
                    function () {
                        var menu = new MenuMorph(
                            myself.deleteVariable,
                            null,
                            myself
                        );
                        myself.deletableVariableNames().forEach(function (name) {
                            menu.addItem(name, name);
                        });
                        menu.popUpAtHand(myself.world());
                    },
                    'Delete a variable'
                );
            button.userMenu = helpMenu;
            button.selector = 'deleteVariable';
            button.showHelp = BlockMorph.prototype.showHelp;
            blocks.push(button);
        }

        blocks.push('-');

        varNames = this.variables.allNames();
        if (varNames.length > 0) {
            varNames.forEach(function (name) {
                blocks.push(variableWatcherToggle(name));
                blocks.push(variableBlock(name));
            });
            blocks.push('-');
        }

        blocks.push(block('doSetVar'));
        blocks.push(block('doChangeVar'));

        blocks.push(block('doDeclareVariables'));

        blocks.push('=');

        blocks.push(block('newArray'));
        blocks.push(block('newByteArray'));
        blocks.push('-');
        blocks.push(block('fillArray'));
        blocks.push('-');
        blocks.push(block('reportListItem'));
        blocks.push('-');
        blocks.push(block('doReplaceInList'));

        blocks.push('=');

        button = new PushButtonMorph(
                null,
                function () {
                    var ide = myself.parentThatIsA(IDE_Morph),
                    stage = myself.parentThatIsA(StageMorph);
                    new BlockDialogMorph(
                        null,
                        function (definition) {
                            if (definition.spec !== '') {
                                if (definition.isGlobal) {
                                    stage.globalBlocks.push(definition);
                                } else {
                                    myself.customBlocks.push(definition);
                                }
                                ide.flushPaletteCache();
                                ide.refreshPalette();
                                new BlockEditorMorph(definition, myself).popUp();
                            }
                        },
                        myself
                        ).prompt(
                            'Make a block',
                            null,
                            myself.world()
                        );
                },
                'Make a block'
            );
        button.userMenu = helpMenu;
        button.selector = 'addCustomBlock';
        button.showHelp = BlockMorph.prototype.showHelp;
        blocks.push(button);
    }
    return blocks;
};

DeviceMorph.prototype.addVariable = function (name, isGlobal) {
    var ide = this.parentThatIsA(IDE_Morph),
        varIndex = this.variables.names().length;
    DeviceMorph.uber.addVariable.call(this, name, isGlobal);
    ide.postal.sendMessage(
        this.portPath,
        'varName',
        varIndex,
        ide.postal.protocol.packString(name)
    );
};

DeviceMorph.prototype.drawNew = function () {
    var ctx, w, h, r;

    this.image = newCanvas(this.extent());

    ctx = this.image.getContext('2d');
    h = this.image.height;
    w = h*2/3;
    r = h/28;

    ctx.save();
    ctx.fillStyle = 'rgba(200,200,200,1)';
    ctx.beginPath();
    ctx.moveTo(0.632*w, 0.012*h);
    ctx.bezierCurveTo(0.562*w, 0.013*h, 0.342*w, 0.046*h, 0.342*w, 0.165*h);
    ctx.bezierCurveTo(0.342*w, 0.241*h, 0.356*w, 0.337*h, 0.392*w, 0.401*h);
    ctx.bezierCurveTo(0.316*w, 0.405*h, 0.299*w, 0.410*h, 0.240*w, 0.417*h);
    ctx.bezierCurveTo(0.282*w, 0.365*h, 0.298*w, 0.313*h, 0.298*w, 0.251*h);
    ctx.bezierCurveTo(0.298*w, 0.029*h, 0.390*w, 0.013*h, 0.344*w, 0.013*h);
    ctx.bezierCurveTo(0.298*w, 0.013*h, 0.035*w, 0.087*h, 0.054*w, 0.251*h);
    ctx.bezierCurveTo(0.081*w, 0.323*h, 0.104*w, 0.426*h, 0.138*w, 0.474*h);
    ctx.bezierCurveTo(0.077*w, 0.550*h, 0.030*w, 0.620*h, 0.030*w, 0.697*h);
    ctx.bezierCurveTo(0.030*w, 0.864*h, 0.241*w, 1.0*h, 0.503*w, 1.0*h);
    ctx.bezierCurveTo(0.791*w, 1.0*h, 1.0*w, 0.864*h, 1.0*w, 0.697*h);
    ctx.bezierCurveTo(1.0*w, 0.643*h, 0.965*w, 0.395*h, 0.517*w, 0.395*h);
    ctx.bezierCurveTo(0.554*w, 0.331*h, 0.569*w, 0.238*h, 0.569*w, 0.165*h);
    ctx.bezierCurveTo(0.569*w, 0.042*h, 0.695*w, 0.012*h, 0.628*w, 0.012*h);
    ctx.bezierCurveTo(0.630*w, 0.012*h, 0.630*w, 0.012*h, 0.632*w, 0.012*h);
    ctx.closePath();
    ctx.fill();
    ctx.restore();
};

// Watchers

DeviceMorph.prototype.watcherGetterForSelector = function (selector, varName) {
    // Watcher getter factory
    return function () {
        var ide = this.parentThatIsA(IDE_Morph),
            block,
            argIndex = 0,
            args = Array.from(arguments),
            watcher = varName ?
                this.findVariableWatcher(varName) :
                this.watcherFor(ide.stage, selector, args);

        if (!watcher) { return; }
        if (!watcher.id) {
            watcher.id = ide.lastStackId;
            block = varName ?
                this.variableBlock(varName) :
                this.blockForSelector(selector);
            // set input values, if there are any
            block.inputs().forEach(function (each, i) { each.setContents(args[i]); });
            ide.lastStackId += 1;
            ide.postal.sendMessage(
                this.portPath,
                'storeChunk',
                watcher.id,
                new Compiler().bytesFor(block)
            );
        }

        // we send two messages to get the next value, but display the last one
        ide.postal.sendMessage(this.portPath, 'startChunk', watcher.id);

        // check out IDE_Morph >> ublocksDispatcher >> taskReturned to understand where
        // this values come from
        return this.watcherValues[selector + args.join('-')];
    }
};

// Create getters for watchers using the watcherGetterForSelector "getter factory"

[
    'microsOp',
    'millisOp',
    'analogReadOp',
    'digitalReadOp',
    'mbTiltX',
    'mbTiltY',
    'mbTiltY',
    'mbTemp',
    'buttonA',
    'buttonB'].forEach(
        function (selector) {
            DeviceMorph.prototype[selector] =
            DeviceMorph.prototype.watcherGetterForSelector(selector);
        }
    );

DeviceMorph.prototype.toggleVariableWatcher = function (varName, isGlobal) {
    DeviceMorph.uber.toggleVariableWatcher.call(this, varName, isGlobal);
    this.toggleVarUpdate(varName);
};

DeviceMorph.prototype.toggleVarUpdate = function (varName) {
    if (this.showingVariableWatcher(varName)) {
        this.watchedVars.push(varName);
    } else {
        this.watchedVars.splice(this.watchedVars.indexOf(varName), 1);
    }
};

DeviceMorph.prototype.step = function () {
    var myself = this;
    this.watchedVars.forEach(function (varName) {
        myself.watcherGetterForSelector('getVar', varName).call(myself);
    });
};

// DeviceMorph script change propagation

DeviceMorph.prototype.scriptChanged = function (topBlock) {
    // If the script is running, stop, update and restart the
    // changed script on the device
    var ide, codes;
    if (!isNil(topBlock.getHighlight())) {
        ide = this.parentThatIsA(IDE_Morph);
        codes = (new Compiler().bytesFor(topBlock));
        ide.postal.sendMessage(this.portPath, 'stopChunk', topBlock.stackId);
        ide.postal.sendMessage(this.portPath, 'storeChunk', topBlock.stackId, codes);
        ide.postal.sendMessage(this.portPath, 'startChunk', topBlock.stackId);
    }
};

DeviceMorph.prototype.customBlockDefinitionChanged = function (aDefinition) {
    var ide, codes;
    ide = this.parentThatIsA(IDE_Morph);
    codes = new Compiler(this).bytesForCustomBlockDefinition(aDefinition);
    ide.postal.sendMessage(this.portPath, 'storeChunk', aDefinition.stackId, codes);
    // to do: stop and restart all scripts using this function (???)
};

// Postal //////////////////////////////////////////////////
// µBlocks postal service
// I facilitate messaging between the web client and the µBlocks plugin

function Postal (address, onReceive) {
    this.init(address, onReceive);
};

Postal.prototype.init = function (address, ide) {
    this.address = address;
    this.protocol = new Protocol(ide);
    this.ide = ide;
    this.socket = null;

    // If true, we are connecting to a websockets enabled board that expects
    // our messages to be in binary format.
    // If false, we are connecting to a websockets hub that will redirect our
    // messages to a hardware port of some sort, so every message needs to
    // carry a port path payload.
    this.binaryMode = false;
    this.onBinaryConnect = nop; // binary mode connection success callback

    // Variable values waiting for someone to pick up
    this.waitingRecipients = {};

    this.startAutoConnect();
};

Postal.prototype.port = 'ws://localhost:9999/';

Postal.prototype.startAutoConnect = function () {
    var myself = this;
    this.connectInterval = setInterval(function () { myself.initSocket(); }, 3000);
};

Postal.prototype.initSocket = function () {
    var myself = this;

    if (!this.socket) {
        this.socket = new WebSocket(this.address);
        this.socket.binaryType = 'arraybuffer';

        this.socket.addEventListener('open', function() {
            clearInterval(myself.connectInterval);
            myself.connectInterval = null;
            if (myself.binaryMode) {
                // We're connected to a websockets enabled board
                myself.onBinaryConnect.call(myself.ide);
            } else {
                myself.ide.devicebutton.enable();
                myself.ide.devicebutton.hint = 'Add a microBlocks device';
            }
        });

        this.socket.onmessage = function (event) {
            myself.protocol.processRawData(Array.from(new Uint8Array(event.data)));
        };

        this.socket.onclose = function () {
            myself.socket = null;
            clearInterval(myself.connectInterval);
            myself.ide.devicebutton.disable();
            myself.ide.devicebutton.hint =
                'Cannot reach microBlocks connector.\nPlease make sure it is running.'
                myself.startAutoConnect();
        };
    }
};

Postal.prototype.rawSend = function (message) {
    if (this.socket && this.socket.readyState === 1) {
        this.socket.send(message);
    }
};

Postal.prototype.sendMessage = function (portPath, selector, taskId, data) {
    var packedMessage;

    if (selector === 'jsonMessage') {
        data = data.split('').map(
            function (char) { return char.charCodeAt(0); }
        );
    }

    packedMessage = this.protocol.packMessage(selector, taskId, data);

    this.rawSend(
        this.binaryMode ?
            new Uint8Array(packedMessage)
                : JSON.stringify({ portPath: portPath, message: packedMessage })
    );
};

Postal.prototype.sendJsonMessage = function (selector, arguments) {
    this.sendMessage(
        null,
        'jsonMessage',
        0,
        JSON.stringify({
            selector: selector,
            arguments: (typeof arguments === 'object') ? arguments : [ arguments ]
        })
    );
};

Postal.prototype.addRecipient = function (recipient, varIndex) {
    if (!this.waitingRecipients[varIndex]) {
        this.waitingRecipients[varIndex] = [];
    }
    this.waitingRecipients[varIndex].push(recipient);
};

Postal.prototype.removeRecipient = function (recipient) {
    var myself = this,
        recipientIndex;
    Object.keys(this.waitingRecipients).forEach(
        function (varIndex) {
            myself.waitingRecipients[varIndex] =
                myself.waitingRecipients[varIndex].filter(
                    function (eachRecipient) {
                        return eachRecipient !== recipient;
                    }
                );
        }
    );
};

// Compiler //////////////////////////////////////////////////
// a simple byte-code compiler for the µblocks VM

function Compiler(receiver) {
    this.receiver = receiver; // for looking up custom block definitions
    this.locals = [];
    this.parameters = [];
}

// Compiler settings

Compiler.prototype.falseObj = 0;
Compiler.prototype.trueObj = 4;
Compiler.prototype.stringClassID = 4;

Compiler.prototype.selectors = {
    // maps Snap's selectors to µblocks' ops
    doPauseAll: 'halt',
    // incrementLocal
    // callFunction
    // returnResult
    // waitMicros
    // waitMillis
    bubble: 'sayIt',
    doStopDevice: 'stopAll',
    reportSum: 'add',
    reportDifference: 'subtract',
    reportProduct: 'multiply',
    reportQuotient: 'divide',
    reportDeviceLessThan: 'lessThan',
	reportDeviceGreaterThan: 'greaterThan',
    reportAnd: 'and',
    reportOr: 'or',
    reportNot: 'notOp',
    reportDeviceEquals: 'equal',
    reportNewList: 'newArray',
    // newByteArray
    // fillArray
    reportListItem: 'at',
    doReplaceInList: 'atPut',
    // analogReadOp
    // analogWriteOp
    // digitalReadOp
    // digitalWriteOp
    // setUserLED
    // microsOp
    // millisOp
    // peek
    // poke
    receiveMessage: 'recvBroadcast',
    doBroadcast: 'sendBroadcast'
};

Compiler.prototype.opcodes = {
    halt: 0,
    noop: 1,
    pushImmediate: 2,
    pushBigImmediate: 3,
    pushLiteral: 4,
    pushVar: 5,
    storeVar: 6,
    incrementVar: 7,
    pushArgCount: 8,
    pushArg: 9,
    storeArg: 10,
    incrementArg: 11,
    pushLocal: 12,
    storeLocal: 13,
    incrementLocal: 14,
    pop: 15,
    jmp: 16,
    jmpTrue: 17,
    jmpFalse: 18,
    decrementAndJmp: 19,
    callFunction: 20,
    returnResult: 21,
    waitMicros: 22,
    waitMillis: 23,
    sendBroadcast: 24,
    recvBroadcast: 25,
    stopAll: 26,
    forLoop: 27,
    initLocals: 28,
    // reserved 29,
    // reserved 30,
    // reserved 31,
    // reserved 32,
    // reserved 33,
    // reserved 34,
    lessThan: 35,
    lessOrEq: 36,
    equal: 37,
    notEqual: 38,
    greaterOrEq: 39,
    greaterThan: 40,
    notOp: 41,
    add: 42,
    subtract: 43,
    multiply: 44,
    divide: 45,
    modulo: 46,
    absoluteValue: 47,
    random: 48,
    hexToInt: 49,
    bitAnd: 50,
    bitOr: 51,
    bitXor: 52,
    bitInvert: 53,
    bitShiftLeft: 54,
    bitShiftRight: 55,
    // reserved 56,
    // reserved 57,
    // reserved 58,
    // reserved 59,
    newArray: 60,
    newByteArray: 61,
    fillArray: 62,
    at: 63,
    atPut: 64,
    size: 65,
    // reserved 66,
    // reserved 67,
    // reserved 68,
    // reserved 69,
    millisOp: 70,
    microsOp: 71,
    peek: 72,
    poke: 73,
    sayIt: 74,
    printIt: 75,
    // reserved 76,
    // reserved 77,
    // reserved 78,
    // reserved 79,
    analogPins: 80,
    digitalPins: 81,
    analogReadOp: 82,
    analogWriteOp: 83,
    digitalReadOp: 84,
    digitalWriteOp: 85,
    digitalSet: 86,
    digitalClear: 87,
    buttonA: 88,
    buttonB: 89,
    setUserLED: 90,
    i2cSet: 91,
    i2cGet: 92,
    spiSend: 93,
    spiRecv: 94,
    // reserved 95,
    // reserved 96,
    // reserved 97,
    // reserved 98,
    // reserved 99,
    // temporary micro:bit primitives for demos:
    mbDisplay: 100,
    mbDisplayOff: 101,
    mbPlot: 102,
    mbUnplot: 103,
    mbTiltX: 104,
    mbTiltY: 105,
    mbTiltZ: 106,
    mbTemp: 107,
    neoPixelSend: 108
};


// Compiler bytecode

Compiler.prototype.bytesFor = function (aBlock, forDefinition) {
    this.locals = aBlock.allLocalVarsInScript();

    var code = this.instructionsFor(aBlock, forDefinition),
        bytes = [this.chunkTypeFor(aBlock)],
        myself = this;

    code.forEach(function (item) {
        if (item instanceof Array) {
            myself.addBytesForInstruction(item, bytes);
        } else if (Number.isInteger(item)) {
            myself.addBytesForInteger(item, bytes);
        } else if (isString(item)) {
            myself.addBytesForStringLiteral(item, bytes);
        } else {
            throw new Error('Instruction must be an Array or String', item);
        }
    });
    return bytes;
};

Compiler.prototype.bytesForCustomBlockDefinition = function (aDefinition) {
    this.parameters = Object.keys(aDefinition.declarations);
    var nop;
    if (isNil(aDefinition.body)) {
        nop = [
            ['noop', 0],
            ['halt', 0]
        ]
        return nop;
    }
    return this.bytesFor(aDefinition.body.expression, true);
};

Compiler.prototype.chunkTypeFor = function (aBlock) {
    if (aBlock.selector === 'receiveGo') {
        return 4;
    } else if (aBlock.selector === 'whenReceiveCondition') {
        return 5;
    } else if (aBlock instanceof ReporterBlockMorph) {
        return 2;
    } else if (aBlock instanceof CommandBlockMorph) {
        return 1;
    }
};

Compiler.prototype.addBytesForInstruction = function (instr, bytes) {
    // append the bytes for the given instruction to bytes (little endian)
    var arg = instr[1];
    bytes.push(this.opcodes[instr[0]]);
    bytes.push(arg & 255);
    bytes.push((arg >> 8) & 255);
    bytes.push((arg >> 16) & 255);
};

Compiler.prototype.addBytesForInteger = function (n, bytes) {
    // append the bytes for the given instruction to bytes (little endian)
    bytes.push(n & 255);
    bytes.push((n >> 8) & 255);
    bytes.push((n >> 16) & 255);
    bytes.push((n >> 24) & 255);
};

Compiler.prototype.addBytesForStringLiteral = function (s, bytes) {
    // append the bytes for the given string to bytes
    var byteCount = this.byteCount(s),
        wordCount = this.wordCount(s),
        headerWord = (wordCount << 4) | this.stringClassID,
        i;
    for (i = 0; i < 4; i += 1) { // repeat 4 times
        bytes.push(headerWord & 255);
        headerWord = (headerWord >> 8);
    }
    bytes.push.apply(bytes, this.str2bytes(s));
    for (i = 0; i < (4 - (byteCount % 4)); i += 1) {
        bytes.push(0); // pad with zeroes to next word boundary
    }
};

// Compiler instructions

Compiler.prototype.instructionsFor = function (aBlock, forDefinition) {
    // return an array of instructions for a stack of command blocks
    // or a reporter. Add a "halt" if needed and append any literals
    // (e.g. strings) used.
    var result = [];
    result.push(['initLocals', this.locals.length]);
    if (aBlock instanceof CommandBlockMorph) {
        result.push.apply(result, this.instructionsForCommandList(aBlock));
        if (forDefinition) {
            result.push(['pushImmediate', this.falseObj])
            result.push(['returnResult', 0]);
        }
    } else if (aBlock instanceof ReporterBlockMorph) {
        // wrap a return statement around the reporter
        result.push.apply(result, this.instructionsForExpression(aBlock));
        result.push(['returnResult', 0]);
    }
    if (!forDefinition) {
        result.push(['halt', 0]);
    }
    if (result.length === 2 &&
        (contains(['halt', 'stopAll', 'doStop', 'doStopDevice'], result[0][0]))
    ) {
        // In general, just looking at the final instruction isn't enough
        // because it could just be the end of a conditional body that is
        // jumped over; in that case, we need the final halt as the jump target.
        result.pop(); // remove the final halt
    }
    this.appendLiterals(result);
    return result;
};

Compiler.prototype.instructionsForCommandList = function (aCmdBlock) {
    var result = [],
        cmd = aCmdBlock;
    while (!isNil(cmd)) {
        result.push.apply(result, this.instructionsForCommand(cmd));
        cmd = cmd.nextBlock();
    }
    return result;
};

Compiler.prototype.instructionsForCommand = function (aBlock) {
    var result = [],
        op = this.selectors[aBlock.selector] || aBlock.selector,
        args = aBlock.inputs(),
        varIndex,
        varName,
        scope;

    switch (op) {
    case 'doSetVar':
        result.push.apply(result, this.instructionsForExpression(args[1]));
        varName = args[0].evaluate();
        scope = aBlock.deviceVarScope(this.parameters);
        switch (scope) {
        case 'global':
            result.push(['storeVar', this.globalVarIndex(varName)]);
            break
        case 'local':
            varIndex = this.locals.indexOf(varName);
            if (varIndex < 0) {
                throw new Error('undeclared local variable:', varName);
            }
            result.push(['storeLocal', varIndex]);
            break
        case 'parameter':
            varIndex = this.parameters.indexOf(varName);
            if (varIndex < 0) {
                throw new Error('undeclared parameter:', varName);
            }
            result.push(['storeArg', varIndex]);
            break
        default:
            throw new Error('Unsupported variable scope:', scope);
        }
        break;
    case 'doChangeVar':
        result.push.apply(result, this.instructionsForExpression(args[1]));

        varName = args[0].evaluate();
        scope = aBlock.deviceVarScope(this.parameters);
        switch (scope) {
        case 'global':
            result.push(['incrementVar', this.globalVarIndex(varName)]);
            break
        case 'local':
            varIndex = this.locals.indexOf(varName);
            if (varIndex < 0) {
                throw new Error('undeclared local variable:', varName);
            }
            result.push(['incrementLocal', varIndex]);
            break
        case 'parameter':
            varIndex = this.parameters.indexOf(varName);
            if (varIndex < 0) {
                throw new Error('undeclared parameter:', varName);
            }
            result.push(['incrementArg', varIndex]);
            break
        default:
            throw new Error('Unsupported variable scope:', scope);
        }
        break;
        break;
    case 'doReport':
        result.push.apply(result, this.instructionsForExpression(args[0]));
        result.push(['returnResult', 0]);
        break;
    case 'doIf':
        return this.instructionsForIf(args);
    case 'doIfElse':
        return this.instructionsForIfElse(args);
    case 'doForever':
        return this.instructionsForForever(args);
    case 'doRepeat':
        return this.instructionsForRepeat(args);
    case 'doWhile':
        return this.instructionsForWhile(args);
    case 'doUntil':
        return this.instructionsForUntil(args);
    case 'doWaitUntil':
    	return this.instructionsForWaitUntil(args);
    case 'doFor':
        return this.instructionsForForLoop(args);
    case 'whenReceiveCondition':
        return this.instructionsForWhenCondition(
            args.concat([aBlock.nextBlock()])
        );
    case 'receiveGo':
        return [];
    case 'receiveMessage':
        return this.instructionsForReceiveMessage(args);
    case 'digitalWriteOp':
        if ((args[0] instanceof ArgMorph) && (args[1] instanceof ArgMorph)) {
            var arg0 = args[0].evaluate();
            var arg1 = args[1].evaluate();
            if (Number.isInteger(arg0) && ((true === arg1) || (false === arg1))) {
                // optimize code when both the pin number and the output boolean are constants
                var pinNum = arg0 & 255;
                if (arg1) {
                    return [['digitalSet', pinNum]];
                } else {
                    return [['digitalClear', pinNum]];
                }
            }
        }
        return this.primitive(op, args, true);
    case 'evaluateCustomBlock':
        return this.instructionsForCustomBlock(aBlock, true);
    default:
        return this.primitive(op, args, true);
    }
    return result;
};

Compiler.prototype.instructionsForIf = function (inputs) {
    var test = inputs[0],
        script = inputs[1].evaluate(), // nested block inside the C-slot
        body = this.instructionsForCommandList(script),
        result = [];
    if (test instanceof ArgMorph) {
        if (test.evaluate()) {
            result.push.apply(result, body);
        }
    } else { // reporter
        result.push.apply(result, this.instructionsForExpression(test));
        result.push(['jmpFalse', body.length]);
        result.push.apply(result, body);
    }
    return result;
};

Compiler.prototype.instructionsForIfElse = function (inputs) {
    // +++ under construction: "else" case
    var test = inputs[0],
        trueBody = this.instructionsForCommandList(inputs[1].evaluate()),
        falseBody = this.instructionsForCommandList(inputs[2].evaluate()),
        result = [];
    if (test instanceof ArgMorph) {
        if (test.evaluate()) {
            result.push.apply(result, trueBody);
        } else {
            result.push.apply(result, falseBody);
        }
    } else { // reporter
        result.push.apply(result, this.instructionsForExpression(test));
        result.push(['jmpFalse', trueBody.length + 1]);
        result.push.apply(result, trueBody);
        result.push(['jmp', falseBody.length]);
        result.push.apply(result, falseBody);
    }
    return result;
};

Compiler.prototype.instructionsForForever = function (inputs) {
    var script = inputs[0].evaluate(), // nested block inside the C-slot
        result = this.instructionsForCommandList(script);
    result.push(['jmp', -(result.length + 1)]);
    return result;
};

Compiler.prototype.instructionsForRepeat = function (inputs) {
    var script = inputs[1].evaluate(), // nested block inside the C-slot
        result = this.instructionsForExpression(inputs[0]), // count
        body = this.instructionsForCommandList(script),
        oneObj = 3; // the integer 1 encoded as an object
    result.push(['pushImmediate', oneObj]);
    result.push(['add', 2]);
    result.push(['jmp', body.length]);
    result.push.apply(result,body);
    result.push(['decrementAndJmp', -(body.length + 1)]);
    return result;
};

Compiler.prototype.instructionsForWhile = function (inputs) {
    var script = inputs[1].evaluate(), // nested block inside the C-slot
        conditionTest = this.instructionsForExpression(inputs[0]), // count
        body = this.instructionsForCommandList(script),
        result = [];

	// +++ to do: special case, when condition is constant TRUE

	result.push(['jmp', body.length]);
 	result.push.apply(result, body);
  	result.push.apply(result, conditionTest);
   	result.push(['jmpTrue', -(body.length + conditionTest.length + 1)]);
    return result;
};

Compiler.prototype.instructionsForUntil = function (inputs) {
    var script = inputs[1].evaluate(), // nested block inside the C-slot
        conditionTest = this.instructionsForExpression(inputs[0]), // count
        body = this.instructionsForCommandList(script),
        result = [];

    // +++ to do: special case, when condition is constant TRUE

    result.push(['jmp', body.length]);
    result.push.apply(result, body);
    result.push.apply(result, conditionTest);
    result.push(['jmpFalse', -(body.length + conditionTest.length + 1)]);
    return result;
};

Compiler.prototype.instructionsForWaitUntil = function (inputs) {
    var conditionTest = this.instructionsForExpression(inputs[0]),
        result = [];
    result.push.apply(result, conditionTest);
    result.push(['jmpFalse', -(conditionTest.length + 1)]);
    return result;
};

Compiler.prototype.instructionsForForLoop = function (inputs) {
    var result = this.instructionsForExpression(inputs[1]),
        loopVarIndex = this.locals.indexOf(inputs[0].contents()),
        body = this.instructionsForCommandList(inputs[2].evaluate());
    result.push.apply(result, [
        ['pushImmediate', this.falseObj], // this will be N, the total loop count
        ['pushImmediate', this.falseObj], // this will be a decrementing loop counter
        ['jmp', body.length]
    ]);
    result.push.apply(result, body);
    result.push.apply(result, [
        ['forLoop', loopVarIndex],
        ['jmp', -(body.length + 2)],
        ['pop', 3]
    ]);
    return result;
};

Compiler.prototype.instructionsForWhenCondition = function (inputs) {
    var condition = this.instructionsForExpression(inputs[0]),
        body = this.instructionsForCommandList(inputs[1]),
        result = [];

    // wait until condition becomes true
    result.push.apply(result, this.instructionsForInteger(10));
    result.push(['waitMillis', 1]);
    result.push.apply(result, condition);
    result.push(['jmpFalse', -(condition.length + 3)]);

    result.push.apply(result, body);

    // loop back to condition test
    result.push(['jmp', -(result.length + 1)]);
    return result;
};

Compiler.prototype.instructionsForReceiveMessage = function (inputs) {
    var result = this.instructionsForExpression(inputs[0]),
        body = this.instructionsForCommandList()
    result.push(['recvBroadcast', 1]);
    return result;
}

Compiler.prototype.instructionsForExpression = function (expr) {
    var value, type, op, args, test1, test2, instrCount, varName, varIndex,
        result;

    // immediate values
    if (expr instanceof ArgMorph) {
        value = expr.evaluate();
        if (value === true) {
            return [['pushImmediate', this.trueObj]];
        }
        if (value === false) {
            return [['pushImmediate', this.falseObj]];
        }
        if (isNil(value)) {
            return [['pushImmediate', 1]]; // the integer zero
        }

        type = Process.prototype.reportTypeOf(value);
        if (type === 'number') {
            value = parseInt(value) || 0;
            if (Number.isInteger(value)) {
                if ((-4194304 <= value) && (value <= 4194303)) {
                    // fits in 23 bits
                    return [[
                        'pushImmediate',
                        ((value << 1) | 1) & 0xFFFFFF // value
                    ]];
                } else if ((-1073741824 <= value) && (value <= 1073741823)) {
                    return [
                        // 32-bit integer objects
                        // follows pushBigImmediate instruction
                        ['pushBigImmediate', 0],
                        (value << 1) | 1
                    ];
                } else {
                    throw new Error('µBlocks only supports integers between -1073741824 and 1073741823');
                }
            } else {
                throw new Error('Unsupported number type:', value);
            }
        } else if (type === 'text') {
            return [['pushLiteral', value]];
        } else {
            throw new Error('Unsupported input type:', type);
        }
    }

    // expressions
    if (expr instanceof ReporterBlockMorph) {
        op = this.selectors[expr.selector] || expr.selector;
        args = expr.inputs();
        if (op === 'reportGetVar') {
            varName = expr.blockSpec;
            scope = expr.deviceVarScope(this.parameters);
            switch (scope) {
            case 'global':
                return [['pushVar', this.globalVarIndex(varName)]];
            case 'local':
                varIndex = this.locals.indexOf(varName);
                if (varIndex < 0) {
                    throw new Error('undeclared local variable:', varName);
                }
                return [['pushLocal', varIndex]];
            case 'parameter':
                varIndex = this.parameters.indexOf(varName);
                if (varIndex < 0) {
                    throw new Error('undeclared parameter:', varName);
                }
                return [['pushArg', varIndex]];
            default:
                throw new Error('Unsupported variable scope:', scope);
            }
        }
        if (op === 'reportBoolean') {
            if (args[0].evaluate()) {
                return [['pushImmediate', this.trueObj]];
            }
            return [['pushImmediate', this.falseObj]];
        }
        if (op === 'and') {
            test1 = this.instructionsForExpression(args[0]);
            test2 = this.instructionsForExpression(args[1]);
            instrCount = 5 + test1.length + test2.length;
            result = [];
            result.push.apply(result, test1);
            result.push(['jmpFalse', instrCount - (result.length + 2)]);
            result.push.apply(result, test2);
            result.push(['jmpFalse', instrCount - (result.length + 2)]);
            result.push(['pushImmediate', this.trueObj]);
            result.push(['jmp', 1]);
            result.push(['pushImmediate', this.falseObj]);
            return result;
        }
        if (op === 'or') {
            test1 = this.instructionsForExpression(args[0]);
            test2 = this.instructionsForExpression(args[1]);
            instrCount = 5 + test1.length + test2.length;
            result = [];
            result.push.apply(result, test1);
            result.push(['jmpTrue', instrCount - (result.length + 2)]);
            result.push.apply(result, test2);
            result.push(['jmpTrue', instrCount - (result.length + 2)]);
            result.push(['pushImmediate', this.falseObj]);
            result.push(['jmp', 1]);
            result.push(['pushImmediate', this.trueObj]);
            return result;
        }
        if (op === 'at') {
             // reverse order of arguments for MicroBlocks
             return this.primitive(op, args.slice().reverse(), false);
        }
        if (op === 'evaluateCustomBlock') {
            return this.instructionsForCustomBlock(expr, false);
        }
        return this.primitive(op, args, false);
    }
    throw new Error('Unknown expression type:', expr);
};

Compiler.prototype.instructionsForInteger = function (value) {
    if (Number.isInteger(value)) {
        if ((-4194304 <= value) && (value < 4194303)) {
            return [[
                'pushImmediate',
                ((value << 1) | 1) & 0xFFFFFF // value
            ]];
        } else if ((-1073741824 <= value) && (value <= 1073741823)) {
            return [
                // 32-bit integer objects
                // follows pushBigImmediate instruction
                ['pushBigImmediate', 0],
                (value << 1) | 1
            ];
        } else {
            throw new Error('µBlocks only supports integers between -1073741824 and 1073741823');
        }
    }
    throw new Error('invalid integer:', value);
};

Compiler.prototype.primitive = function (op, args, isCommand) {
    var result = [],
        myself = this,
        shift, displayWord

	if (op === 'mbDisplay') {
		shift = 0;
  		displayWord = 0;
    	args.forEach(function (bit) {
            if (bit instanceof BooleanSlotMorph && (bit.evaluate() === true)) {
            	displayWord = displayWord | (1 << shift);
            }
            shift += 1;
        });
        result.push.apply(result, (this.instructionsForInteger(displayWord)));
		result.push(['mbDisplay', 1]);
		return result;
 	}
 	if (op === 'atPut') {
        // change order of first two arguments for MicroBlocks
        args = args.slice();
        var tmp = args[0];
        args[0] = args[1];
        args[1] = tmp;
    }
 	if (this.opcodes[op]) {
        args.forEach(function (input) {
            result.push.apply(
                result,
                myself.instructionsForExpression(input)
            );
        });
        result.push([op, args.length]);
    } else {
        console.log('Skipping unknown op:', op);
    }
    return result;
};

Compiler.prototype.globalVarIndex = function (varName) {
    var id = MicroBlocksGlobals[varName];
    if (id === undefined) {
        id = Object.keys(MicroBlocksGlobals).length;
        MicroBlocksGlobals[varName] = id;
    }
    return id;
};

Compiler.prototype.instructionsForCustomBlock = function (aBlock, isCmd) {
    var result = [],
        args = aBlock.inputs(),
        definition = this.receiver.getMethod(aBlock.semanticSpec),
        params = Object.keys(definition.declarations),
        myself = this,
        callee = definition.stackId;
    args.forEach(function (input) {
        result.push.apply(
            result,
            myself.instructionsForExpression(input)
        );
    });
    result.push(['callFunction', (((callee & 255) << 8) | (args.length & 255))]);
    if (isCmd) {
        // discard the return value
        result.push(['pop', 1]);
    }
    return result;
}

Compiler.prototype.appendLiterals = function (instructions) {
    // for now, strings are the only literals.
    // May add literal arrays later.
    var literals = [],
        literalOffsets = {},
        nextOffset = instructions.length,
        myself = this;
    instructions.forEach(function (instr, ip) {
        if (instr instanceof Array && (instr[0] === 'pushLiteral')) {
            var literal = (instr[1]),
                litOffset = literalOffsets[literal];
            if (isNil(litOffset)) {
                litOffset = nextOffset;
                literals.push(literal);
                literalOffsets[literal] = litOffset;
                nextOffset += myself.wordsForLiteral(literal);
            }
            instr[1] = litOffset - (ip + 1); // b/c JS has zero offset arrays
        }
    });
    instructions.push.apply(instructions, literals);
};

Compiler.prototype.wordsForLiteral = function (literal) {
    var headerWords = 1;
    if (isString(literal)) {
        return headerWords + this.wordCount(literal);
    }
    throw new Error('Illegal literal type:', literal);
};

// Compiler conversion

Compiler.prototype.str2bytes = function (str) {
    var bytes = [], i, n;
    for(i = 0, n = str.length; i < n; i += 1) {
        var char = str.charCodeAt(i);
        // bytes.push(char >>> 8, char & 0xFF);
        bytes.push(char & 0x7F);
    }
    return bytes;
};

Compiler.prototype.bytes2str = function (bytes) {
    var chars = [], i, n;
    for(i = 0, n = bytes.length; i < n;) {
        chars.push(((bytes[i++] & 0xff) << 8) | (bytes[i++] & 0xff));
    }
    return String.fromCharCode.apply(null, chars);
};


Compiler.prototype.wordCount = function(aString) {
    return Math.floor((this.byteCount(aString) + 4) / 4);
};

Compiler.prototype.byteCount = function(aString) {
    return this.str2bytes(aString).length;
    // return encodeURI(aString).split(/%..|./).length - 1;
};
